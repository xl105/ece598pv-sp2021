use super::message::Message;
use std::time::{SystemTime};
use super::peer;
use crate::network::server::Handle as ServerHandle;
use crate::blockchain::{self, *};
use crate::block::{self, *};
use crate::transaction::{self, *};
use crate::crypto::hash::{H256, Hashable};
use serde::{Serialize, Deserialize};

use std::collections::HashMap;
use crossbeam::channel;
use log::{debug, warn};

use std::sync::{Arc, Mutex};
use std::thread;

#[derive(Clone)]
pub struct Context {
    msg_chan: channel::Receiver<(Vec<u8>, peer::Handle)>,
    num_worker: usize,
    server: ServerHandle,
    blockchain: Arc<Mutex<Blockchain>>,
    orphan_buffer:Arc<Mutex<HashMap<H256, Block>>>,
    tx_mempool:Arc<Mutex<HashMap<H256, SignedTransaction>>>,
    block_state: Arc<Mutex<HashMap<H256, State>>>,
}

pub fn new(
    num_worker: usize,
    msg_src: channel::Receiver<(Vec<u8>, peer::Handle)>,
    server: &ServerHandle,
    blockchain: &Arc<Mutex<Blockchain>>,
    orphan_buffer:&Arc<Mutex<HashMap<H256, Block>>>,
    tx_mempool:&Arc<Mutex<HashMap<H256, SignedTransaction>>>,
    block_state: &Arc<Mutex<HashMap<H256, State>>>
) -> Context {
    Context {
        msg_chan: msg_src,
        num_worker,
        server: server.clone(),
        blockchain: Arc::clone(blockchain),
        orphan_buffer: Arc::clone(orphan_buffer),
        tx_mempool: Arc::clone(tx_mempool),
        block_state: Arc::clone(block_state)
    }
}

impl Context {
    pub fn start(self) {
        let num_worker = self.num_worker;
        for i in 0..num_worker {
            let cloned = self.clone();
            thread::spawn(move || {
                cloned.worker_loop();
                warn!("Worker thread {} exited", i);
            });
        }
    }

    fn worker_loop(&self) {
        loop {
            let msg = self.msg_chan.recv().unwrap();
            let (msg, peer) = msg;
            let msg: Message = bincode::deserialize(&msg).unwrap();
            let mut lock_thread = self.blockchain.lock().unwrap();
            let mut orphan_buffer = self.orphan_buffer.lock().unwrap(); 
            let mut tx_mempool = self.tx_mempool.lock().unwrap();
            match msg {
                Message::Ping(nonce) => {
                    debug!("Ping: {}", nonce);
                    peer.write(Message::Pong(nonce.to_string()));
                }
                Message::Pong(nonce) => {
                    debug!("Pong: {}", nonce);
                }
                Message::NewBlockHashes(hashes) => {
                    let mut get_blocks:Vec<H256> = vec![];
                    
                    for hash in hashes {
                        if !lock_thread.blockchain.contains_key(&hash) {
                            get_blocks.push(hash);
                        }
                    }

                    if get_blocks.len() > 0 {
                        peer.write(Message::GetBlocks(get_blocks));
                    }
                }
                Message::GetBlocks(hashes) => {
                    let mut ret_blocks:Vec<Block> = vec![];

                    for hash in hashes {
                        for (bhash,blk) in lock_thread.blockchain.iter() {
                            if *bhash == hash {
                                ret_blocks.push(blk.clone());
                            }
                        }
                    }

                    if ret_blocks.len() > 0 {
                        peer.write(Message::Blocks(ret_blocks));
                    }
                }
                Message::Blocks(ret_blocks) => {
                    let mut rec_hash:Vec<H256> = vec![];
                    let mut mis_hash:Vec<H256> = vec![];
                    for blk in ret_blocks {
                        if !lock_thread.blockchain.contains_key(&blk.header.parent) {
                            mis_hash.push(blk.header.parent);
                            orphan_buffer.insert(blk.hash(), blk.clone());
                            println!("Parent not found!");
                            continue;
                        }
                        if blk.header.difficulty != lock_thread.blockchain.get(&blk.header.parent).unwrap().header.difficulty {
                            continue;
                        }
                        if !block_check(&blk, &block_state) {
                            continue;
                        }

                        lock_thread.insert(&blk.clone());
                        state_update(&blk, &block_state);
                        
                        if blk.hash() == lock_thread.tip {
                            for tx in blk.content.content.iter() {
                                let tx_hash = tx.hash(); 
                                if tx_mempool.contains_key(&tx_hash) {
                                   tx_mempool.remove(&tx_hash); 
                                }
                            }
                        }

                        rec_hash.push(blk.hash());
                        let delay: u128 = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis() - blk.header.timestamp;
                        let size = bincode::serialize(&blk).unwrap().len();
                        println!("blocklength: {}", lock_thread.longest);
                        println!("delay: {}",delay);
                        println!("size: {}", size);
                    }
                    loop {
                        let mut counter = 0;
                        let mut remove:Vec<H256> = vec![];
                        for (_,blk) in orphan_buffer.iter() {
                            if lock_thread.blockchain.contains_key(&blk.header.parent){
                                if blk.header.difficulty != lock_thread.blockchain.get(&blk.header.parent).unwrap().header.difficulty {
                                    remove.push(blk.hash());
                                    continue;
                                }
                                if !block_check(&blk, &block_state) {
                                    remove.push(blk.hash());
                                    continue;
                                }
                                lock_thread.insert(&blk);
                                state_update(&blk, &block_state);
                                remove.push(blk.hash());
                                counter +=1;
                            }
                        }
                        for hash in remove {
                            orphan_buffer.remove(&hash);
                        }
                        if counter == 0 {
                            break;
                        }
                    }

                    self.server.broadcast(Message::GetBlocks(mis_hash));
                    self.server.broadcast(Message::NewBlockHashes(rec_hash));
                }
                Message::NewTransactionHashes(hashes) => {
                    let mut tx_hashes:Vec<H256> = vec![];
                                                            
                    for hash in hashes {
                        if !tx_mempool.contains_key(&hash) {                             
                            tx_hashes.push(hash);                                                                   
                        }
                    }
                    if tx_hashes.len() > 0 {
                        peer.write(Message::GetTransactions(tx_hashes));                     
                    }
                }
                Message::GetTransactions(hashes) => {
                    let mut ret_tx:Vec<SignedTransaction> = vec![];

                    for hash in hashes {
                        match tx_mempool.get(&hash) {
                            Some(tx) => ret_tx.push(tx.clone()),
                            None => {}
                        }
                    }
                    if ret_tx.len() > 0 {
                        peer.write(Message::Transactions(ret_tx));
                    }
                }
                Message::Transactions(txs) => {
                    let mut tx_hashes: Vec<H256> = vec![];
                    for tx in txs {
                        if tx_check(&tx){
                            let hash = tx.hash();
                            match tx_mempool.get(&hash) {
                                Some(signed_tx) => {},
                                None => {
                                    tx_mempool.insert(tx.hash(), tx.clone());
                                    //tx_hashes
                                }
                            }
                        }
                    } 
                }
            }
        }
    }
}
