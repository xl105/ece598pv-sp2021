use serde::{Serialize,Deserialize};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters};
use crate::crypto::hash::{H256, Hashable};
use rand::Rng;
use rand::distributions::Alphanumeric;
use crate::crypto::key_pair;

#[derive(Serialize, Deserialize, Debug, Default,Clone, Eq, PartialEq, Hash)]
pub struct UtxoInput{
    pub tx_hash: H256,
    pub index: u8,  
}

#[derive(Serialize, Deserialize, Debug, Default,Clone)]
pub struct UtxoOutput{
    pub receipient_addr: H160,
    pub value: u32,
}

#[derive(Serialize, Deserialize, Debug, Default,Clone)]
pub struct State{
   pub content: HashMap<UtxoInput, UtxoOutput>,
}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct Transaction {
    pub input: Vec<UtxoInput>,
    pub output: Vec<UtxoOutput>,
}

impl Hashable for Transaction {
    fn hash(&self) -> H256{
        let encode = bincode::serialize(&self).unwrap();
        ring::digest::digest(&ring::digest::SHA256, &encode).into()
    }
}

#[derive(Serialize, Deserialize, Debug, Default,Clone)]
pub struct SignedTransaction {
    pub transaction: Transaction,
    pub signature: Vec<u8>,
    pub public_key: Vec<u8>
}

impl Hashable for SignedTransaction {
    fn hash(&self) -> H256{
        let encode = bincode::serialize(&self).unwrap();
        ring::digest::digest(&ring::digest::SHA256, &encode).into()
    }
}
/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
    let encode = bincode::serialize(&t).unwrap();
    let sig = key.sign(&encode);
    sig
}

pub fn tx_check(signed_tx: &SignedTransaction) -> bool {
    let encode:Vec<u8> = bincode::serialize(&signed_tx.transaction).unwrap();
    let pk = ring::signature::UnparsedPublicKey::new(&ring::signature::ED25519, &signed_tx.public_key);
    pk.verify(&encode[..], &signed_tx.signature).is_ok()
}

/// Verify digital signature of a transaction, using public key instead of secret key
pub fn verify(t: &Transaction, public_key: &<Ed25519KeyPair as KeyPair>::PublicKey, signature: &Signature) -> bool {
    let encode = bincode::serialize(&t).unwrap();
    let sig = signature.as_ref();
    let pk = ring::signature::UnparsedPublicKey::new(&ring::signature::ED25519, public_key);
    match pk.verify(&encode, sig){
        Ok(_n) => {
            true
        },
        Err(_e) => {
            false
        },
    }
}

pub fn state_update (block: &Block, block_state: &mut HashMap<H256, State>) {
    let mut cur_block_state = &block_state[&block.header.parent].clone();

    for S_tx in &block.content.content.iter() {
        for input in &S_tx.transaction.input {
            cur_block_state.remove(input);
        }
        for (i, output) in &S_tx.transaction.output.iter().enumerate() {
            let input = UtxoInput(tx_hash: S_tx.transaction.hash(), index: i as u8);
            cur_block_state.insert(input, output.clone());
        }
    }
    block_state.insert(block.hash(), cur_block_state);
}

pub fn generate_random_transaction() -> Transaction {
        let mut rng = rand::thread_rng();
        let rand_input: String = rng.sample_iter(&Alphanumeric).take(30).collect();
        let rand_output:String = rng.sample_iter(&Alphanumeric).take(30).collect();
        let transaction = Transaction{
            input: rand_input,
            output: rand_output
        };
        transaction
}

pub fn generate_random_signedtransaction() -> SignedTransaction {
        let transaction = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&transaction, &key);
        let signedtransaction = SignedTransaction{
            transaction: transaction,
            signature: signature.as_ref().to_vec(),
            public_key: key.public_key().as_ref().to_vec()
        };
        signedtransaction
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::crypto::key_pair;

    pub fn generate_random_transaction() -> Transaction {
        let mut rng = rand::thread_rng();
        let rand_input: String = rng.sample_iter(&Alphanumeric).take(30).collect();
        let rand_output:String = rng.sample_iter(&Alphanumeric).take(30).collect();
        let transaction = Transaction{
            input: rand_input,
            output: rand_output
        };
        transaction
    }

    #[test]
    fn sign_verify() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, &(key.public_key()), &signature));
    }
    #[test]
    fn assignment2_transaction_1() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, &(key.public_key()), &signature));
    }
    #[test]
    fn assignment2_transaction_2() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        let key_2 = key_pair::random();
        let t_2 = generate_random_transaction();
        assert!(!verify(&t_2, &(key.public_key()), &signature));
        assert!(!verify(&t, &(key_2.public_key()), &signature));
    }
}
