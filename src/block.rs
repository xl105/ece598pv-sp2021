use std::time::{SystemTime};
use serde::{Serialize, Deserialize};
use ring::signature::{self,Ed25519KeyPair, Signature, KeyPair};
use crate::crypto::hash::{self, *};
use crate::transaction::{self, *};
use crate::crypto::key_pair;
use hex_literal::hex;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Header {
    pub parent: H256,
    pub nonce: u32,
    pub difficulty: H256,
    pub timestamp: u128,
    pub merkle_root: H256
}

impl Hashable for Header {
    fn hash(&self) -> H256 {
        let encode = bincode::serialize(&self).unwrap();
        ring::digest::digest(&ring::digest::SHA256, &encode).into()
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Content {
    pub content: Vec<SignedTransaction>
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Block {
    pub header: Header,
    pub content: Content
}

impl Hashable for Block {
    fn hash(&self) -> H256 {
        self.header.hash()
    }
}

pub fn block_check(block: &Block, block_state: HashMap<H256, State>) -> bool {
    if block.hash() > block.header.difficulty {
        return false;
    }
    for S_tx in &block.content.content.iter() {
        if !tx_chech(&S_tx){
            return false;
        }
        let receipient_addr:H160 = H160::From(&S_tx.public_key);
        let mut total_input = 0;
        for input in &S_tx.transaction.input {
            if !block_state[&block.header.parent].contains_key(&input){
                return false;
            }
            let output = &block_state[&block.header.parent].content[&input];
            if output.receipient_addr != receipient_addr {
                return false;
            }
            total_input += output.value;
        }
        let mut total_output = 0;
        for output in &S_tx.transaction.output {
            total_output += output.value;
        }
        if total_intput != total_output {
            return false;
        }
    }
    return true;
}

pub fn generate_genensis_block() -> Block {
    let key = let key = Ed25519KeyPair::from_pkcs8([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1].as_ref().into()).unwrap();
    let address:H160 = H160::From(public_key);
    let tx_hash:H256 = hex!("ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff").into() ;
    let value:u32 = 100000000;
    let input = vec![UtxoInput{tx.hash: tx_hash, index: 0 as u8}];
    let output = vec!UtxoOutput{receipient_addr: address, value: value}];
    let transaction = Transaction{input: input, output: output};
    let signature = sign(&transaction, &key);
    let signedtransaction = SignedTransaction{
        transaction: transaction,
        signature:signature.as_ref().to_vec(),
        public_key: key.public_key().as_ref().to_vec()
    };
    let difficulty:H256 = hex!("0007ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff").into();
    let mut gen:[u8;32] = [255;32];
    for i in 0..2{
        gen[i] = 0;
    }
    let parent:[u8;32] = [0;32];
    let mkroot:H256 = signedtransaction.hash();
    let header = Header{
        parent: <H256>::from(parent),
        nonce: 7,
        //difficulty: <H256>::from(gen),
        difficulty: difficulty,
        timestamp: 7,
        merkle_root:<H256>::from(parent) 
    };
    let mut cont:Vec<SignedTransaction> = Vec::new();
    cont.push(signedtransaction);
    let content = Content {
        content: cont
    };
    Block{
        header: header,
        content: content
    }
}

pub fn ICO() -> State{
    let key = let key = Ed25519KeyPair::from_pkcs8([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1].as_ref().into()).unwrap();
    let public = key.public_key().as_ref().to_vec();
    let address:H160 = H160::From(public_key);
    let tx_hash:H256 = hex!("ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff").into() ;
    let value:u32 = 100000000;
    let mut state:State = State{content: HashMap::new()};
    let input = UtxoInput{tx.hash: tx_hash, index: 0 as u8};
    let output = UtxoOutput{receipient_addr: address, value: value};
    state.content.insert(UtxoInput, UtxoOutput);
    return state
}

#[cfg(any(test, test_utilities))]
pub mod test {
    use super::*;
    use crate::crypto::hash::H256;

    pub fn generate_random_block(parent: &H256) -> Block {
        let signedtransaction = transaction::generate_random_signedtransaction();
        let gen:[u8;32] = [10;32];
        let mkroot:H256 = signedtransaction.hash();
        let header = Header{
                parent: *parent,
                nonce: rand::random::<u32>(),
                difficulty: <H256>::from(gen),
                timestamp: SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis(),
                merkle_root: mkroot
        };
        let mut cont:Vec<SignedTransaction> = Vec::new();
        cont.push(signedtransaction);
        let content = Content {
            content: cont
        };
        Block{
            header: header,
            content: content
        }
    }
}
