use crate::network::server::Handle as ServerHandle;
use crate::transaction::{self,*};
use crate::crypto::merkle::{self, *};
use crate::blockchain::*;
use crate::block::*;
use std::time::{SystemTime};
use std::sync::{Arc, Mutex};
use crate::crypto::hash::{H256, Hashable};
use crate::network::message::Message;

use std::collections::HashMap;
use log::info;
use rand::Rng;
use crossbeam::channel::{unbounded, Receiver, Sender, TryRecvError};
use std::time;

use std::thread;

enum ControlSignal {
    Start(u64), // the number controls the lambda of interval between block generation
    Exit,
}

enum OperatingState {
    Paused,
    Run(u64),
    ShutDown,
}

pub struct Context {
    /// Channel for receiving control signal
    control_chan: Receiver<ControlSignal>,
    operating_state: OperatingState,
    server: ServerHandle,
    blockchain: Arc<Mutex<Blockchain>>,
    orphan_buffer: Arc<Mutex<HashMap<H256, Block>>>,
    tx_mempool: Arc<Mutex<HashMap<H256, SignedTransaction>>>,
    block_state: Arc<Mutex<HashMap<H256, State>>>,,
}

#[derive(Clone)]
pub struct Handle {
    /// Channel for sending signal to the miner thread
    control_chan: Sender<ControlSignal>,
}

pub fn new(
    server: &ServerHandle,
    blockchain: &Arc<Mutex<Blockchain>>,
    orphan_buffer: &Arc<Mutex<HashMap<H256, Block>>>,
    tx_mempool:&Arc<Mutex<HashMap<H256, SignedTransaction>>>,
    block_state: &Arc<Mutex<HashMap<H256, State>>>
) -> (Context, Handle) {
    let (signal_chan_sender, signal_chan_receiver) = unbounded();

    let ctx = Context {
        control_chan: signal_chan_receiver,
        operating_state: OperatingState::Paused,
        server: server.clone(),
        blockchain: Arc::clone(blockchain),
        orphan_buffer: Arc::clone(orphan_buffer),
        tx_mempool: Arc::clone(tx_mempool),
        block_state: Arc::clone(block_state)
    };

    let handle = Handle {
        control_chan: signal_chan_sender,
    };

    (ctx, handle)
}

impl Handle {
    pub fn exit(&self) {
        self.control_chan.send(ControlSignal::Exit).unwrap();
    }

    pub fn start(&self, lambda: u64) {
        self.control_chan
            .send(ControlSignal::Start(lambda))
            .unwrap();
    }

}

impl Context {
    pub fn start(mut self) {
        thread::Builder::new()
            .name("miner".to_string())
            .spawn(move || {
                self.miner_loop();
            })
            .unwrap();
        info!("Miner initialized into paused mode");
    }

    fn handle_control_signal(&mut self, signal: ControlSignal) {
        match signal {
            ControlSignal::Exit => {
                info!("Miner shutting down");
                self.operating_state = OperatingState::ShutDown;
            }
            ControlSignal::Start(i) => {
                info!("Miner starting in continuous mode with lambda {}", i);
                self.operating_state = OperatingState::Run(i);
            }
        }
    }

    fn miner_loop(&mut self) {
        let mut minedblock:u32 = 0;
        // main mining loop
        loop {
            // check and react to control signals
            match self.operating_state {
                OperatingState::Paused => {
                    let signal = self.control_chan.recv().unwrap();
                    self.handle_control_signal(signal);
                    continue;
                }
                OperatingState::ShutDown => {
                    return;
                }
                _ => match self.control_chan.try_recv() {
                    Ok(signal) => {
                        self.handle_control_signal(signal);
                    }
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => panic!("Miner control channel detached"),
                },
            }
            if let OperatingState::ShutDown = self.operating_state {
                return;
            }

            // TODO: actual mining
            let mut lock_thread = self.blockchain.lock().unwrap();
            let mut tx_mempool = self.tx_mempool.lock().unwrap();
            let parenthash = lock_thread.tip();

            let mut rng = rand::thread_rng();
            let nonce = rng.gen();

            let timestamp = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_millis();
            let difficulty = lock_thread.blockchain.get(&lock_thread.tip).unwrap().header.difficulty;

            let mut signed_trans:Vec<SignedTransaction> = vec![];
            let mut merkle_data:Vec<H256> = vec![];
            let mut counter = 0;
            for (key, value) in tx_mempool.iter() {
                if !tx_check(&value) {
                    continue;
                }
                if 
                signed_trans.push(value.clone());
                merkle_data.push(key.clone());
                counter += 1;
                if counter >= 20 {
                    break;
                }
            }
            let mut merkle_tree = MerkleTree::new(&merkle_data);
            let merkle_root = merkle_tree.root();

            let content = Content{
                content: signed_trans
            };

            let header = Header{
                parent: parenthash,
                nonce: nonce,
                difficulty: difficulty,
                timestamp: timestamp,
                merkle_root: merkle_root
            };

            let block = Block{
                header: header,
                content:content
            };

            if block_check(&block){
                minedblock +=1;
                println!{"minedblock:{}\n",minedblock};
                println!("time:{}\n", timestamp); 
                lock_thread.insert(&block);
                state_update(&blk, &block_state);
                for tx in block.content.content.iter() {
                    let tx_hash = tx.hash();
                    if tx_mempool.contains_key(&tx_hash) {
                        tx_mempool.remove(&tx_hash);
                    }
                }
                let mut mined_hash: Vec<H256> = vec![];
                mined_hash.push(block.hash());
                self.server.broadcast(Message::NewBlockHashes(mined_hash));
            }

            if let OperatingState::Run(i) = self.operating_state {
                if i != 0 {
                    let interval = time::Duration::from_micros(i as u64);
                    thread::sleep(interval);
                }
            }

            std::mem::drop(lock_thread);
        }
    }
}
