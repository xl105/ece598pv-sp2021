use super::hash::{Hashable, H256};
use ring::digest::{Context, SHA256};
use std::collections::VecDeque;

/// A Merkle tree.
#[derive(Debug, Default)]
pub struct MerkleTree {
    mtree: Vec<H256>,
    datalength: usize
}

impl MerkleTree {
    pub fn new<T>(data: &[T]) -> Self where T: Hashable, {
        let mut m_tree:Vec<H256> = Vec::new();
        let mut DQue:VecDeque<H256> = VecDeque::new();

        let mut data_length = data.len();

        for n in 0..data_length {
            DQue.push_back(data[n].hash());
        }

        if data_length%2 != 0{
            DQue.push_back(data[data_length-1].hash());
            data_length = data_length+1;
        }

        let mut round = data_length / 2;

        while round > 0{
            let mut parent:H256 = H256::from([0;32]);
            for _n in 0..round {
                let left = DQue.pop_front().unwrap();
                m_tree.push(left);

                let right = DQue.pop_front().unwrap();
                m_tree.push(right);

                let mut hasher = Context::new(&SHA256);
                hasher.update(&<[u8;32]>::from(left));
                hasher.update(&<[u8;32]>::from(right));
                parent = H256::from(hasher.finish());

                DQue.push_back(parent);
            }
            if round % 2 != 0 && round != 1{
                DQue.push_back(parent);
                round = round + 1;
            }
            if round == 1{
                m_tree.push(parent);
            }
            round /= 2 
        }
        MerkleTree{mtree: m_tree, datalength: data_length}
    }

    pub fn root(&self) -> H256 {
        self.mtree[self.mtree.len()-1]
    }

    /// Returns the Merkle Proof of data at index i
    pub fn proof(&self, index: usize) -> Vec<H256> {
        let mut pf:Vec<H256> = Vec::new();
        let mut i = index;
        let mut length = self.datalength;
        let mut base = 0;
        while base + 1< self.mtree.len(){
            if i % 2 == 0 {
                pf.push(self.mtree[base+i+1]);
            }else {
                pf.push(self.mtree[base+i-1]);
            }

            i = i / 2;
            base += length;
            if length % 2 !=0 {
                length += 1;
            }
            length /= 2;
        }
        pf
    }
}

/// Verify that the datum hash with a vector of proofs will produce the Merkle root. Also need the
/// index of datum and `leaf_size`, the total number of leaves.
pub fn verify(root: &H256, datum: &H256, proof: &[H256], index: usize, leaf_size: usize) -> bool {
    let mut data = *datum;
    let mut i = index;
    for n in 0..proof.len(){
        let mut hasher = Context::new(&SHA256);
        if i % 2 == 0{
            hasher.update(&<[u8;32]>::from(data));
            hasher.update(&<[u8;32]>::from(proof[n]));
        }else {
            hasher.update(&<[u8;32]>::from(proof[n]));
            hasher.update(&<[u8;32]>::from(data));
        }
        data = H256::from(hasher.finish());
        i /= 2; 
    }
    if *root == data{
        true
    }else {
        false
    }
}

#[cfg(test)]
mod tests {
    use crate::crypto::hash::H256;
    use super::*;

    macro_rules! gen_merkle_tree_data {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
            ]
        }};
    }

    macro_rules! gen_merkle_tree_assignment2 {
        () => {{
            vec![
                (hex!("0000000000000000000000000000000000000000000000000000000000000011")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000022")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000033")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000044")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000055")).into(),                                      (hex!("0000000000000000000000000000000000000000000000000000000000000066")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000077")).into(),
                (hex!("0000000000000000000000000000000000000000000000000000000000000088")).into(),
            ]
        }};
    }

    macro_rules! gen_merkle_tree_assignment2_another {
        () => {{
            vec![
                (hex!("1000000000000000000000000000000000000000000000000000000000000088")).into(),
                (hex!("2000000000000000000000000000000000000000000000000000000000000077")).into(),
                (hex!("3000000000000000000000000000000000000000000000000000000000000066")).into(),                     
                (hex!("4000000000000000000000000000000000000000000000000000000000000055")).into(),                     
                (hex!("5000000000000000000000000000000000000000000000000000000000000044")).into(),         
                (hex!("6000000000000000000000000000000000000000000000000000000000000033")).into(),
                (hex!("7000000000000000000000000000000000000000000000000000000000000022")).into(),
                (hex!("8000000000000000000000000000000000000000000000000000000000000011")).into(),                      
            ]
        }};
    }

    #[test]
    fn assignment2_merkle_root() {
        let input_data: Vec<H256> = gen_merkle_tree_assignment2!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6e18c8441bc8b0d1f0d4dc442c0d82ff2b4f38e2d7ca487c92e6db435d820a10")).into()
        );
    }

    #[test]
    fn assignment2_merkle_verify() {
        let input_data: Vec<H256> = gen_merkle_tree_assignment2!();
        let merkle_tree = MerkleTree::new(&input_data);
        for i in 0.. input_data.len() {
            let proof = merkle_tree.proof(i);
            assert!(verify(&merkle_tree.root(), &input_data[i].hash(), &proof, i, input_data.len()));
        }
        let input_data_2: Vec<H256> = gen_merkle_tree_assignment2_another!();
        let merkle_tree_2 = MerkleTree::new(&input_data_2);
        assert!(!verify(&merkle_tree.root(), &input_data[0].hash(), &merkle_tree_2.proof(0), 0, input_data.len()));
    }
  
    #[test]
    fn assignment2_merkle_proof() {
        use std::collections::HashSet;
        let input_data: Vec<H256> = gen_merkle_tree_assignment2!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(5);
        let proof: HashSet<H256> = proof.into_iter().collect();
        let p: H256 = (hex!("c8c37c89fcc6ee7f5e8237d2b7ed8c17640c154f8d7751c774719b2b82040c76")).into();
        assert!(proof.contains(&p));
        let p: H256 = (hex!("bada70a695501195fb5ad950a5a41c02c0f9c449a918937267710a0425151b77")).into();
        assert!(proof.contains(&p));
        let p: H256 = (hex!("1e28fb71415f259bd4b0b3b98d67a1240b4f3bed5923aa222c5fdbd97c8fb002")).into();
        assert!(proof.contains(&p));
    }

    #[test]
    fn root() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920")).into()
        );
        // "b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0" is the hash of
        // "0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d"
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
        // "6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920" is the hash of
        // the concatenation of these two hashes "b69..." and "965..."
        // notice that the order of these two matters
    }

    #[test]
    fn proof() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert_eq!(proof,
                   vec![hex!("965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f").into()]
        );
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
    }

    #[test]
    fn verifying() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert!(verify(&merkle_tree.root(), &input_data[0].hash(), &proof, 0, input_data.len()));
    }
}
