use crate::block::{self, *};
use std::collections::HashMap;
use crate::crypto::hash::{H256, Hashable};

pub struct Blockchain {
    pub blockchain: HashMap<H256, Block>,
    pub tip: H256,
    pub length: HashMap<H256,u128>,
    pub longest: u128
}

impl Blockchain {
    /// Create a new blockchain, only containing the genesis block
    pub fn new() -> Self {
        let genensis:Block = block::generate_genensis_block();
        let tip:H256 = genensis.hash();
        let mut len:HashMap<H256, u128> = HashMap::new();
        len.insert(tip, 0);
        let mut blockchain:HashMap<H256, Block> = HashMap::new();
        blockchain.insert(tip, genensis);
        Blockchain {
            blockchain: blockchain,
            tip: tip,
            length: len,
            longest: 0
        }
    }

    /// Insert a block into blockchain
    pub fn insert(&mut self, block: &Block) {
        let hash = block.hash();
        let len = self.length[&block.header.parent] + 1;
        if len > self.longest {
            self.tip = hash;
            self.longest = len;
        }
        self.length.insert(hash, len);
        self.blockchain.insert(hash, block.clone());
    }

    /// Get the last block's hash of the longest chain
    pub fn tip(&self) -> H256 {
        self.tip
    }

    /// Get the last block's hash of the longest chain
    #[cfg(any(test, test_utilities))]
    pub fn all_blocks_in_longest_chain(&self) -> Vec<H256> {
        let mut longest_chain: Vec<H256> = Vec::new();
        let mut cur = self.tip;

        if self.longest == 0 {
            longest_chain.push(cur);
            return longest_chain;
        }

        for _n in 0..self.longest {
            longest_chain.push(cur);
            cur = self.blockchain[&cur].header.parent;
        }

        longest_chain.reverse();
        longest_chain
    }
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::block::test::generate_random_block;
    use crate::crypto::hash::Hashable;

    #[test]
    fn insert_one() {
        let mut blockchain = Blockchain::new();
        let genesis_hash = blockchain.tip();
        let block = generate_random_block(&genesis_hash);
        blockchain.insert(&block);
        assert_eq!(blockchain.tip(), block.hash());

    }
}
